/**
 * Created by Jacob Flynn on 02/03/2017, all content and code copyright Jacob Flynn © 2017
 */

$(function () {
    //variable states screen must be no smaller than 654 pixels
    var mq = window.matchMedia( "(min-width: 654px)" );
    //if screen is more than that
    if (mq.matches) {
        function myFunction(x) {
            x.classList.toggle("change");
        }

        //then once each website image has the mouse enter it
        $('.wrapper > a:nth-child(1) > img').mouseenter(function () {
            //fade in the read more button
            $('.wrapper > a:nth-child(1) > .skillstext').fadeIn(100, function () {
                //when the mouse leaves
                $('.skill').mouseleave(function () {
                    //fade out the button
                    $('.skillstext').fadeOut(100);
                });
            });
        });

        //then once each website image has the mouse enter it
        $('.wrapper > a:nth-child(2) > img').mouseenter(function () {
            //fade in the read more button
            $('.wrapper > a:nth-child(2) > .skillstext').fadeIn(100, function () {
                //when the mouse leaves
                $('.skill').mouseleave(function () {
                    //fade out the button
                    $('.skillstext').fadeOut(100);
                });
            });
        });

        //then once each website image has the mouse enter it
        $('.wrapper > a:nth-child(3) > img').mouseenter(function () {
            //fade in the read more button
            $('.wrapper > a:nth-child(3) > .skillstext').fadeIn(100, function () {
                //when the mouse leaves
                $('.skill').mouseleave(function () {
                    //fade out the button
                    $('.skillstext').fadeOut(100);
                });
            });
        });
    }

    //when the showall skill is clicked add the class clicked to it and remove it from everything else
    $('#showall').click(function () {
        $(this).fadeIn("slow", function () {
            $(this).addClass('clicked');
            $("#website").removeClass('clicked');
            $("#jquery").removeClass('clicked');
            $("#javascript").removeClass('clicked');
            $("#mysql").removeClass('clicked');
            $("#php").removeClass('clicked');
            $("#server").removeClass('clicked');
            $("#laravel").removeClass('clicked');
            $("#sass").removeClass('clicked');
            $("#wordpress").removeClass('clicked');
            //then fade in or out the related website
            $("#cleversteam").fadeIn();
            $("#lookamazing").fadeIn();
            $("#seetheuniverse").fadeIn();
            $("#bekkaharveymakeup").fadeIn();
        })
    });

    //when the jquery skill is clicked add the class clicked to it and remove it from everything else
    $('#jquery').click(function () {
        $(this).fadeIn("slow", function () {
            $(this).addClass('clicked');
            $("#showall").removeClass('clicked');
            $("#website").removeClass('clicked');
            $("#javascript").removeClass('clicked');
            $("#mysql").removeClass('clicked');
            $("#php").removeClass('clicked');
            $("#server").removeClass('clicked');
            $("#laravel").removeClass('clicked');
            $("#sass").removeClass('clicked');
            $("#wordpress").removeClass('clicked');
            //then fade in or out the related website
            $("#seetheuniverse").hide();
            $("#cleversteam").fadeIn();
            $("#lookamazing").fadeIn();
            $("#bekkaharveymakeup").hide();
        })
    });

    //when the javascript skill is clicked add the class clicked to it and remove it from everything else
    $('#javascript').click(function () {
        $(this).fadeIn("slow", function () {
            $(this).addClass('clicked');
            $("#showall").removeClass('clicked');
            $("#website").removeClass('clicked');
            $("#jquery").removeClass('clicked');
            $("#mysql").removeClass('clicked');
            $("#php").removeClass('clicked');
            $("#server").removeClass('clicked');
            $("#laravel").removeClass('clicked');
            $("#sass").removeClass('clicked');
            $("#wordpress").removeClass('clicked');
            //then fade in or out the related website
            $("#seetheuniverse").hide();
            $("#cleversteam").fadeIn();
            $("#lookamazing").fadeIn();
            $("#bekkaharveymakeup").fadeIn();
        })
    });

    //when the mysql skill is clicked add the class clicked to it and remove it from everything else
    $('#mysql').click(function () {
        $(this).fadeIn("slow", function () {
            $(this).addClass('clicked');
            $("#showall").removeClass('clicked');
            $("#website").removeClass('clicked');
            $("#jquery").removeClass('clicked');
            $("#javascript").removeClass('clicked');
            $("#php").removeClass('clicked');
            $("#server").removeClass('clicked');
            $("#laravel").removeClass('clicked');
            $("#sass").removeClass('clicked');
            $("#wordpress").removeClass('clicked');
            //then fade in or out the related website
            $("#cleversteam").hide();
            $("#lookamazing").hide();
            $("#seetheuniverse").fadeIn();
            $("#bekkaharveymakeup").hide();
        })
    });

    //when the php skill is clicked add the class clicked to it and remove it from everything else
    $('#php').click(function () {
        $(this).fadeIn("slow", function () {
            $(this).addClass('clicked');
            $("#showall").removeClass('clicked');
            $("#website").removeClass('clicked');
            $("#jquery").removeClass('clicked');
            $("#javascript").removeClass('clicked');
            $("#mysql").removeClass('clicked');
            $("#server").removeClass('clicked');
            $("#laravel").removeClass('clicked');
            $("#sass").removeClass('clicked');
            $("#wordpress").removeClass('clicked');
            //then fade in or out the related website
            $("#cleversteam").hide();
            $("#lookamazing").hide();
            $("#seetheuniverse").fadeIn();
            $("#bekkaharveymakeup").fadeIn();
        })
    });

    //when the laravel skill is clicked add the class clicked to it and remove it from everything else
    $('#laravel').click(function () {
        $(this).fadeIn("slow", function () {
            $(this).addClass('clicked');
            $("#showall").removeClass('clicked');
            $("#website").removeClass('clicked');
            $("#jquery").removeClass('clicked');
            $("#javascript").removeClass('clicked');
            $("#mysql").removeClass('clicked');
            $("#php").removeClass('clicked');
            $("#server").removeClass('clicked');
            $("#sass").removeClass('clicked');
            $("#wordpress").removeClass('clicked');
            //then fade in or out the related website
            $("#cleversteam").hide();
            $("#lookamazing").hide();
            $("#seetheuniverse").fadeIn();
            $("#bekkaharveymakeup").hide();
        })
    });

    //when the sass skill is clicked add the class clicked to it and remove it from everything else
    $('#sass').click(function () {
        $(this).fadeIn("slow", function () {
            $(this).addClass('clicked');
            $("#showall").removeClass('clicked');
            $("#website").removeClass('clicked');
            $("#jquery").removeClass('clicked');
            $("#javascript").removeClass('clicked');
            $("#mysql").removeClass('clicked');
            $("#php").removeClass('clicked');
            $("#server").removeClass('clicked');
            $("#laravel").removeClass('clicked');
            $("#wordpress").removeClass('clicked');
            //then fade in or out the related website
            $("#cleversteam").fadeIn();
            $("#lookamazing").fadeIn();
            $("#seetheuniverse").fadeIn();
            $("#bekkaharveymakeup").hide();
        })
    });

    $('#wordpress').click(function () {
        $(this).fadeIn("slow", function () {
            $(this).addClass('clicked');
            $("#showall").removeClass('clicked');
            $("#website").removeClass('clicked');
            $("#jquery").removeClass('clicked');
            $("#javascript").removeClass('clicked');
            $("#mysql").removeClass('clicked');
            $("#php").removeClass('clicked');
            $("#server").removeClass('clicked');
            $("#laravel").removeClass('clicked');
            //then fade in or out the related website
            $("#cleversteam").hide();
            $("#lookamazing").hide();
            $("#seetheuniverse").fadeIn();
            $("#bekkaharveymakeup").hide();
        })
    });
});