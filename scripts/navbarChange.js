/**
 * Created by Jacob Flynn on 02/03/2017, all content and code copyright Jacob Flynn © 2017
 */
$(document).ready(function () {
    //when the down arrow image is clicked
    $('#downarrow').click(function () {
        $("body, html").animate({
            //scroll the webpage down by 600ms to the about section
            scrollTop: $("#about").offset().top
        }, 600);
    });
});

//variable states screen must be no smaller than 654 pixels
var mq = window.matchMedia( "(min-width: 654px)" );
//if it is not
if (mq.matches) {
    $(document).ready(function () {
        //then if the webpage is scroll down below 20 pixels
        $(window).scroll(function () {
            if ($(document).scrollTop() > 20) {
                //then add scrolled class to the navbar and fade the navbar in and increase font size to the first link
                $("nav > ul").addClass("scrolled");
                $("nav > ul > li").addClass("scrolled");
                $("nav > ul > li:nth-child(2)").fadeIn();
                $("nav > ul > li:nth-child(3)").fadeIn();
                $("nav > ul > li:nth-child(4)").fadeIn();
                $("nav > ul > li:nth-child(5)").fadeIn();
                $("nav > ul > li:nth-child(6)").fadeIn();
                $("nav > ul > li:first-child").css("font-size", "20px");

            } else {
                //if webpage has not scrolled below 20 pixels then remove class scrolled and fade navbar out
                $("nav > ul").removeClass("scrolled");
                $("nav > ul > li").removeClass("scrolled");
                $("nav > ul > li:nth-child(2)").fadeOut();
                $("nav > ul > li:nth-child(3)").fadeOut();
                $("nav > ul > li:nth-child(4)").fadeOut();
                $("nav > ul > li:nth-child(5)").fadeOut();
                $("nav > ul > li:nth-child(6)").fadeOut();
                $("nav > ul > li:first-child").css("font-size", "30px");

            }
        });
    });
}
