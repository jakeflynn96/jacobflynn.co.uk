/**
 * Created by Jacob Flynn on 02/03/2017, all content and code copyright Jacob Flynn © 2017
 */

//array of words declared as 'things I do'
var thingsido = [
    "websites",
    "user interfaces",
    "platforms",
    "web applications"
];

//pull the span tag through from the html file named 'things i do'
var getSpanTag = document.getElementById("thingsido");

//function called changeword
function changeWord() {
    getSpanTag.innerHTML = thingsido[0]; // push the first key in the span tag
    thingsido.push(thingsido.shift());    // then Push the first key to the end of list.
}
changeWord();                  // First run of the function
setInterval(changeWord, 1500); // loop over the function at 1500ms