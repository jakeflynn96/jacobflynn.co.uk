/**
 * Created by Jacob Flynn on 02/03/2017, all content and code copyright Jacob Flynn © 2017
 */
$(document).ready(function () {
    /*when the short story button is clicked*/
    $('#shortstorybutton').click(function () {
        /*and it is not already displayed*/
        if ($('#shortstory').css('display') === 'none') {
            /*then slide the short story down and the long story up*/
            $('#shortstory').slideDown();
            $('#longstory').slideUp();
        } else {
            /*if it is then just slide the short story up*/
            $('#shortstory').slideUp();

        }
    });

    /*when the longstory button is clicked*/
    $('#longstorybutton').click(function () {
        /*and it is not already displayed*/
        if ($('#longstory').css('display') === 'none') {
            /*then slide the long story down and the short story up*/
            $('#longstory').slideDown();
            $('#shortstory').slideUp();
        } else {
            /*if not then just slide the long story up*/
            $('#longstory').slideUp();
        }
    });
});